/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad1_array;

/**
 *
 * @author hitzu
 */
public class STARWAR {
     public static void main(String[] args) {
        // TODO code application logic here
        String Personajes [][] = new String [4][4];
        Personajes [0][0]= "Luke Skywalker";
        Personajes [0][1]= "R2-D2";
        Personajes [0][2]= "C-3PO";
        Personajes [0][3]= "Darth Vader";
        Personajes [1][0]= "Leia Organa";
        Personajes [1][1]= "Owen Lars";
        Personajes [1][2]= "Beru Whitesun Lars";
        Personajes [1][3]= "R5-D4";
        Personajes [2][0]= "Biggs Darklighter";
        Personajes [2][1]= "Obi-Wan Kenobi";
        Personajes [2][2]= "Yoda";
        Personajes [2][3]= "Jek Tono Porkins";
        Personajes [3][0]= "Jabba Desilijic Tiure";
        Personajes [3][1]= "Han Solo";
        Personajes [3][2]= "Chewbacca";
        Personajes [3][3]= "Anakin Skywalker";
        
        System.out.println("\nPersonajes de Star Wars (ciclo for simple): ");
        for(int i=0;i<4;i++){
        System.out.println(java.util.Arrays.toString(Personajes[i]));
        }
        
        System.out.println("\nPersonajes de Star Wars (ciclo for each): ");
        for(String[] i:Personajes){
        System.out.println(java.util.Arrays.toString(i));}
        
        
        System.out.println("");
    }
}
